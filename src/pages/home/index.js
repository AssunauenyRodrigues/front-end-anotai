import React from 'react';
import './styles.css';
import '../../styles-global/styles.css';
import LogoOrange from '../../assets/logo-orange.svg'
import Header from '../../components/header';
import ptLocale from "date-fns/locale/pt-BR";
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import OrderCard from '../../components/orderCard';
import api from '../../services/api';
import { withRouter } from 'react-router-dom';

const currentDate = new Date;

class Home extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            orderList: [],
            daySearch: currentDate.getDate() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getFullYear(),
            dayComponent: new Date()
        }
        this.getOrderList = this.getOrderList.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.updateDate = this.updateDate.bind(this);
    }

    getOrderList(date){
        if(!date){
            date = currentDate.getDate() + '-' + (currentDate.getMonth() + 1) + '-' + currentDate.getFullYear()
        }
        let body = {
            loja_id: '111.111.111-11',
            date: date
        }
        api.post('demand/filter/today', body)
            .then((resp) => {
                this.setState({
                    orderList: resp.data
                })
            })
            .catch((error) => {
                console.log(error.response)
            })
    }

    updateDate(e){
        
    }

    onChangeDate(e){
        let day = 0;
        if(e.getDate() < 10){
            day = "0" + e.getDate()
        }
        else {
            day = e.getDate()
        }
        this.setState({
            dayComponent: new Date(e.getFullYear(), e.getMonth(), e.getDate())
        })
        let date = e.getDate()  + '-' + (e.getMonth() + 1) + '-' + e.getFullYear()
        this.getOrderList(date)
    }

    componentDidMount(){
        this.getOrderList()
    }

    render(){
        return(
            <div>
                <Header/>
                <div className="container-home">
                    <div className="container-date">
                        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ptLocale}>
                            <DatePicker
                                label="Data selecionada"
                                animateYearScrolling
                                format="dd/MM/yyyy"
                                value={this.state.dayComponent}
                                onChange={(e) => { this.onChangeDate(e) } }
                                maxDate={ new Date() }
                            />
                        </MuiPickersUtilsProvider>                        
                    </div>
                    <div className="container-tittle">
                        <h2 style={{color:'#a5b1c2'}}>Lista de pedidos</h2>
                    </div>
                    <div style={{textAlign: 'center'}}>
                        {this.state.orderList.length === 0 &&
                            <p style={{color: 'red'}} >Não há pedidos cadastrados para o dia selecionado! :(</p> 
                        }
                    </div>
                    <div className="container-list-card">
                        {   
                            this.state.orderList.map((element, index) => (
                                <OrderCard idDemand={element._id} key={index} client={element.client} title={element.title} description={element.description} street={element.street} number={element.number} neighboard={element.neighboard} public_place={element.public_place}  />
                            ))
                        }
                    </div>
                </div>
            </div>
        )
    }

}

export default withRouter(Home);