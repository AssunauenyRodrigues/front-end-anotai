import React from 'react';
import './styles.css';
import '../../styles-global/styles.css';
import LogoOrange from '../../assets/logo-orange.svg'
import { withRouter } from 'react-router-dom';

class Login extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            cpf: '',
            password: ''
        }
        this.login = this.login.bind(this);
    }

    cpfCnpj(v) {

        v.replace(/\D/g, '');
    
        if (v.length <= 14) { //CPF
    
            v = v.replace(/\D/g, "");
            v = v.replace(/(\d{3})(\d)/, "$1.$2")
            v = v.replace(/(\d{3})(\d)/, "$1.$2")
            v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2")
    
        }
        return v
    }
    
    login(){
        if(this.state.cpf === '111.111.111-11' && this.state.password === 'abcd'){
            this.props.history.push('/inicio')
        }else{
             alert("Cpf ou senha inválido!")
        }
    }

    render(){
        return(
            <div className="container">
                <div className="card-login">
                    <img style={{marginBottom: '20px'}} src={LogoOrange} alt="logo"></img>
                    <input style={{borderRadius: '8px'}} onInput={(e) => {this.setState({ cpf: this.cpfCnpj(e.target.value) })}} value={this.state.cpf} maxLength={14} placeholder="CPF"></input>
                    <input style={{borderRadius: '8px'}} onInput={(e) => {this.setState({ password: e.target.value })}} placeholder='Senha' type="password"></input>
                    <button onClick={this.login} className="button-login">Entrar</button>
                </div>
            </div>
        )
    }

}

export default withRouter(Login)