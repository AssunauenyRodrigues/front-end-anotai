import React from 'react';
import './styles.css';
import '../../styles-global/styles.css';
import LogoDark from '../../assets/logo-dark.svg';
import ModalNewDemand from '../modalNewDemand';

export default class Header extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            modalNewDemand: false
        }
    }

    render(){
        return(
            <div className="container-header">
                {this.state.modalNewDemand && 
                    <ModalNewDemand/>
                }
                <div className="header-left">
                    <img width="130px" src={LogoDark} alt="logo"></img>
                </div>
                <div className="header-right">
                    <a style={{ color: '#353B48', background: 'white', borderRadius: '8px', fontWeight: 'bolder'}} href="/inicio">INÍCIO</a>
                    <span style={{color: 'white'}}>|</span>
                    <button onClick={() => { this.setState({ modalNewDemand: true }) }} className="button-new-demand">NOVO PEDIDO</button>
                    <span style={{color: 'white'}}>|</span>
                    <a href="/">SAIR</a>
                </div>
            </div>
        )
    }

}