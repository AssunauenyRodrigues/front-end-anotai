import React from 'react';
import './styles.css';
import '../../styles-global/styles.css';
import UpdateIcon from '../../assets/update-icon.svg';
import DeleteIcon from '../../assets/delete-icon.svg';
import ModalUpdateDemand from '../modalUpdateDemand';
import ModalDeleteDemand from '../modelDeleteConfirmation';

export default class OrderCard extends React.Component{

    constructor(idDemand, client, title, description, street, number, neighboard, public_place){
        super(idDemand, client, title, description, street, number, neighboard, public_place);
        this.state = {
            showModalUpdateDemand: false,
            showModalDeleteDemand: false
        }
    }

    render(){
        return(
            <div className="order-card">
                {this.state.showModalUpdateDemand &&
                    <ModalUpdateDemand idDemand={this.props.idDemand} client={this.props.client} title={this.props.title} description={this.props.description} street={this.props.street} number={this.props.number} neighboard={this.props.neighboard} public_place={this.props.public_place}/>
                }
                {this.state.showModalDeleteDemand &&
                    <ModalDeleteDemand idDemand={this.props.idDemand} title={this.props.title} />
                }
                <div style={{textAlign: "center"}}>
                <h1>{this.props.title}</h1>
                </div>
                <div className="content-data-card-order">
                    <label>CLIENTE:</label>
                    <h4>{this.props.client}</h4>
                    <label>DESCRIÇÃO DO PEDIDO:</label>
                    <h4 style={{marginBottom: '3vw'}} >{this.props.description}</h4>
                    <label>ENDEREÇO DE ENTREGA:</label>
                    <h4>{this.props.street}</h4>
                </div>
                <div style={{textAlign: 'right', paddingRight: '10px', paddingBottom: '5px'}}>
                    <button style={{border: 'none', background: 'none'}} onClick={( ) => { this.setState({ showModalUpdateDemand: true }) }} ><img width="30px" src={UpdateIcon}></img></button>
                    <button style={{border: 'none', background: 'none'}} onClick={( ) => { this.setState({ showModalDeleteDemand: true }) }} ><img width="30px" src={DeleteIcon}></img></button>
                </div>
            </div>
        )
    }

}