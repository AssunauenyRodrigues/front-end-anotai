import React from 'react';
import './styles.css';
import '../../styles-global/styles.css';
import Modal from 'react-modal';
import api from '../../services/api';

export default class ModalDeleteDemand extends React.Component{

    constructor(idDemand, title){
        super(idDemand, title);
        this.state = {
            showModal: true,

            formSubmited: false,
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleSubmit(){
        api.delete('demand/' + this.props.idDemand)
            .then((resp) => {
                alert("Excluído com sucesso")
                window.location.href = "/inicio"
            })
            .catch((error) => {
                alert(error.response)
            })
    }

    handleCancel(){
        this.setState({
            showModal: false
        })
        window.location.href = "/inicio"
    }

    render(){
        return(
            <div>
                <Modal
                  isOpen={this.state.showModal}
                  contentLabel="Example Modal"
                  className="modalDeleteDemand"
                >
                    <div className="title-delete-demand">
                        <h3>Excluindo pedido</h3>
                    </div>
                    <div className="body-form-delete-demand">
                        <h3>Pedido a ser excluído:</h3>
                        <h2>{this.props.title}</h2>
                    </div>
                    <div className="footer-form-delete-demand">
                        <button className="button-save" onClick={this.handleCancel}>Cancelar</button>
                        <button className="button-cancel" onClick={this.handleSubmit}>Confirmar</button>
                    </div>
                </Modal>
            </div>
        )
    }

}