import React from 'react';
import './styles.css';
import '../../styles-global/styles.css';
import Modal from 'react-modal';
import api from '../../services/api';

export default class ModalUpdateDemand extends React.Component{

    constructor(idDemand, client, title, description, street, number, neighboard, public_place){
        super(idDemand, client, title, description, street, number, neighboard, public_place);
        this.state = {
            showModal: true,

            formSubmited: false,

            client: this.props.client,
            title: this.props.title,
            description: this.props.description,
            street: this.props.street,
            number: this.props.number,
            neighboard: this.props.neighboard,
            public_place: this.props.public_place
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    handleSubmit(){
        this.setState({formSubmited: true})
        let body = {
            client: this.state.client,
            title: this.state.title,
            description: this.state.description,
            street: this.state.street,
            number: this.state.number,
            neighboard: this.state.neighboard,
            public_place: this.state.public_place
        }
        api.put('demand/' + this.props.idDemand , body)
            .then((resp) => {
                alert("Atualizado com sucesso")
                window.location.href = "/inicio"
            })
            .catch((error) => {
                alert(error.response.data.error)
            })
    }

    handleCancel(){
        this.setState({
            showModal: false
        })
        window.location.href = "/inicio"
    }

    render(){
        return(
            <div>
                <Modal
                  isOpen={this.state.showModal}
                  contentLabel="Example Modal"
                  className="modalUpdateDemand"
                >
                    <div className="title-update-demand">
                        <h3>Editando pedido</h3>
                    </div>
                    <div>
                        <div>
                            <div className="body-form-update-demand">
                                <label>NOME DO CLIENTE:</label>
                                <input value={this.state.client} style={ this.state.formSubmited && this.state.client === '' ? {borderBottom: '1px solid red'} : {borderBottom: '1px solid #000000'}} onChange={ (e) => { this.setState({ client: e.target.value }) } }/>
                                <label>NOME DO PEDIDO:</label>
                                <input value={this.state.title} style={ this.state.formSubmited && this.state.title === '' ? {borderBottom: '1px solid red'} : {borderBottom: '1px solid #000000'}} onChange={ (e) => { this.setState({ title: e.target.value }) } }/>
                                <label>DESCRIÇÃO DO PEDIDO:</label>
                                <input value={this.state.description} style={ this.state.formSubmited && this.state.description === '' ? {borderBottom: '1px solid red'} : {borderBottom: '1px solid #000000'}} onChange={ (e) => { this.setState({ description: e.target.value }) } }/>
                                <label>RUA:</label>
                                <input value={this.state.street} style={ this.state.formSubmited && this.state.street === '' ? {borderBottom: '1px solid red'} : {borderBottom: '1px solid #000000'}} onChange={ (e) => { this.setState({ street: e.target.value }) } }/>
                                <label>BAIRRO:</label>
                                <input value={this.state.neighboard} style={ this.state.formSubmited && this.state.neighboard === '' ? {borderBottom: '1px solid red'} : {borderBottom: '1px solid #000000'}} onChange={ (e) => { this.setState({ neighboard: e.target.value }) } }/>
                                <label>NÚMERO:</label>
                                <input value={this.state.number} style={ this.state.formSubmited && this.state.number === '' ? {borderBottom: '1px solid red'} : {borderBottom: '1px solid #000000'}} onChange={ (e) => { this.setState({ number: e.target.value }) } } />
                                <label>COMPLEMENTO:</label>
                                <input value={this.state.public_place} style={ this.state.formSubmited && this.state.public_place === '' ? {borderBottom: '1px solid red'} : {borderBottom: '1px solid #000000'}} onChange={ (e) => { this.setState({ public_place: e.target.value }) } } />
                            </div>
                            <div className="footer-form-update-demand">
                                <button className="button-save" onClick={this.handleCancel}>Cancelar</button>
                                <button className="button-cancel" onClick={this.handleSubmit}>Salvar</button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        )
    }

}